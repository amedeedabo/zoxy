package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/miekg/dns"
)

func handleDNSRequest(w dns.ResponseWriter, r *dns.Msg) {
	m := new(dns.Msg)
	m.SetReply(r)
	m.Authoritative = true
	for _, q := range m.Question {
		switch q.Qtype {
		case dns.TypeA:
			log.Printf("Query for %s\n", q.Name)
			rr, _ := dns.NewRR(fmt.Sprintf("%s A %s", q.Name, "127.0.0.1"))
			m.Answer = append(m.Answer, rr)
		}
	}
	w.WriteMsg(m)
}

func serveDNS(port int) {
	dns.HandleFunc("z.", handleDNSRequest)
	serve := func(proto string) {
		srv := &dns.Server{Addr: ":" + strconv.Itoa(port), Net: proto}
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("Failed to start DNS %s listener %s\n", proto, err.Error())
		}
	}
	go serve("tcp")
	go serve("udp")
}
