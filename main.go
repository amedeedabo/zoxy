package main

import (
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
)

func mapPort(host string) (int, error) {
	destPort, found := DefaultConfig.PortMap[host]
	if !found {
		intPort, err := strconv.Atoi(host)
		if err == nil {
			destPort = intPort
		} else {
			return -1, errors.New("The port was not found in the config map, nor is it an integer")
		}
	}
	return destPort, nil
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}
func reply(resp http.ResponseWriter, status int, content string) {
	resp.WriteHeader(status)
	io.WriteString(resp, content)
}

func proxyPort(resp http.ResponseWriter, req *http.Request) {
	log.Print("Request for ", req.Host)
	host, _, found := strings.Cut(req.Host, ".z")
	if !found {
		reply(resp, http.StatusNotFound, "This server only proxies .z hosts")
		return
	}
	if host == "z" {
		reply(resp, http.StatusOK, "<html><h1>Future zoxy management interface</h1></html>")
		return
	}
	newPort, err := mapPort(host)
	if err != nil {
		reply(resp, http.StatusNotFound, err.Error())
		return
	}
	log.Print("Redirecting to port ", newPort)

	newHost := fmt.Sprintf("localhost:%d", newPort)
	req.Host = newHost
	req.URL.Host = newHost
	req.URL.Scheme = "http"

	proxy.ServeHTTP(resp, req)
}

var proxy = &httputil.ReverseProxy{
	Director: func(r *http.Request) {},
	ErrorHandler: func(rw http.ResponseWriter, r *http.Request, err error) {
		log.Printf("Error from proxied resource: %+v", err)
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte(err.Error()))
	},
}

func main() {
	noDNS := flag.Bool("noDNS", false, "disable the built-in DNS server; likely because you are already using dnsmasq.")
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		fmt.Println("zoxy config subcommand: ")
		fmt.Println("\tconfig print: print the path of the config file.")
		fmt.Println("\tconfig load: read from stdin and write to the config file.")
		fmt.Println("Main command flags:")
		flag.PrintDefaults()
	}

	flag.Parse()
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "config":
			switch os.Args[2] {
			case "path":
				fmt.Print(getConfigPath())
			case "load":
				writeStdinToUserConfig()
			default:
				fmt.Println("Unknown config command ", os.Args[2])
			}
			return
		}
	}
	loadUserConfig()
	http.HandleFunc("/", proxyPort)
	go func() {
		log.Fatal(http.ListenAndServe(":80", nil))
	}()
	if DefaultConfig.DnsPort != -1 && !*noDNS {
		log.Print("Starting DNS server on port ", DefaultConfig.DnsPort)
		serveDNS(DefaultConfig.DnsPort)
	} else {
		log.Print("Skipping starting DNS server")
	}
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	s := <-sig
	log.Fatalf("Signal (%v) received, stopping\n", s)
}
