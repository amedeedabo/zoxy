## Zoxy: forget about localhost:3000

Zoxy gives you short names like `hugo.z` so you don't have to remember `localhost:1313`
or even look up at the number pad.

```
next.z -> localhost:3000
hugo.z -> localhost:1313
jupyter.z -> localhost:8888
```

## Installing

- Mac

```
brew install amedeedaboville/tap/zoxy
# Tell your computer that the .z DNS resolver .z is at 127.0.0.1:53
sudo sh -c "mkdir -p /etc/resolver && echo 'nameserver 127.0.0.1' >> /etc/resolver/z"
brew services start zoxy
```

- Linux
  The linux config has not been tried yet but will be soon.
  ATM you need Go installed, and since Linux doesn't allow domain-specific DNS , you'll also need to set up dnsmasq:

```bash
sudo sh -c "echo $'address=/z/127.0.0.1\nport=53' >> /etc/resolve.conf"
git clone gitlab.com/amedeedabo/zoxy && cd zoxy
sudo go run . -noDNS
```

## Why do I need all this stuff, can't I just use my hosts file?

It'd be really nice if we could just have a hosts file like:

```
hugo.z 127.0.0.1:3030
```

But hosts files are only for resolving domains to IPs and don't let you have port numbers in them.

You could do add this to your hosts file

```
z 127.0.0.1
```

and not have to type out `localhost` every time, but you'd still have to remember port numbers like `z:1313`.

## Configuration and defaults

The default port mappings are bundled into the app and can be seen [here](config.go)
(If a popular app you like isn't on this list, please file an issue!).
You can overwrite the defaults and add your own mappigns by editing a config file located at :

- `$XDG_CONFIG_HOME/zoxy/config.json` if XDG_CONFIG_HOME is set, otherwise at:
- Macos: `~/Library/Application Support/zoxy/config.json`
- Linux: `~/.config/zoxy/config.json`

Zoxy will read this file at startup and will use any port mappings you give as overrides.
For example if you run hugo on port 2000, you can put this in your config.json:

```json
{
  "ports": {
    "hugo": 2000,
    "tensorboard": 6006
  },
   "aliases" {
      "tb": "tensorboard"
   }
}
```

You can also give services shorter names by adding to the `aliases` section of the config.json.
Only a single layer of aliasing is supported (aliases can't point to other aliases).

### My browser googles the url instead of going to app.z

This can happen, as your browser doesn't know that `app.z` is a website that will work and it really likes
to search things. You can get around this by specifying `http://app.z` the first time, then afterward `app.z`
should work every time.

### Extras

- The letters for port 2000,3000,...,9000 are accessible at `two.z`, `three.z`, ..., `nine.z`.
- Any URL which is a number is interpreted as that port and forwarded directly. `8000.z -> localhost:8000`

## How does it work?

Zoxy is composed of two pieces, a local DNS resolver on port 53 and a port-forwarding server on port 80.

1. The .z domain
   This is handled by running our own DNS server for .z (a non-existent, short domain name), which always points to localhost.
   At installation we add a file `/etc/resolver/z` that contains `nameserver 127.0.0.1` to make the local machine use this server.
   On linux we can't add per-domain DNS resolvers, so we have to hack this by using dnsmasq for all queries, and then configuring dnsmasq to use zoxy for .z

2. The subdomains
   This is handled by running a regular http server on port 80 that proxies to the right port by reading the Host header
   (which are the contents of the URL) and looking up the domain in its list of port mappings. It then forwards the request
   to the port it has found.

## What state is it in at the moment?

A handful of developers have downloaded from homebrew and used it on macOS.

After linux support, some future nice-to-haves (MRs welcome or pair with me!) would be:

- zoxy --configPath command to find the config file more easily
- zoxy --install command to write to `/etc/resolve/z` automatically (maybe)
- a web UI for editing `config.json`, probably in VanillaJS (maybe)
- integrating Caddy server for hot reload, TLS and more user extensability (maybe)
- maybe a home page/dashboard that shows which local servers are running
