package main

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"os"
	"path"
	"runtime"
)

type Config struct {
	DnsPort int               `json:"dns_port"`
	PortMap map[string]int    `json:"ports"`
	Aliases map[string]string `json:"aliases"`
}

// Combines two configs by adding all the keys from top into base.
// if the same keys are present in base and top, top wins.
func mergeConfig(base *Config, top *Config) {
	if top.DnsPort != 0 {
		base.DnsPort = top.DnsPort
	}
	//Combine default and user config, with user config overriding
	for k, v := range top.PortMap {
		base.PortMap[k] = v
	}
	for k, v := range top.Aliases {
		base.Aliases[k] = v
	}
}

func getConfigDir() string {
	configPath := os.Getenv("XDG_CONFIG_HOME")
	if configPath == "" {
		home := os.Getenv("HOME")
		if runtime.GOOS == "darwin" {
			configPath = path.Join(home, "/Library/Application Support")
		} else {
			configPath = path.Join(home, ".config")
		}
	}
	return path.Join(configPath, "zozy")
}

func getConfigPath() string {
	return path.Join(getConfigDir(), "config.json")
}

func writeStdinToUserConfig() {
	//create the folder if necessary
	if err := os.MkdirAll(getConfigDir(), os.ModePerm); err != nil {
		log.Fatal(err)
	}
	file, err := os.OpenFile(getConfigPath(), os.O_CREATE|os.O_WRONLY, os.ModePerm)
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}
	io.Copy(file, os.Stdin)

}
func loadUserConfig() {
	var userConfig Config
	configPath := getConfigPath()
	log.Print("Looking for user config in ", configPath)
	contents, err := os.ReadFile(configPath)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			log.Print("User config file does not exist, skipping it.")
			return
		} else {
			log.Print("Error opening user config file ", err)
		}
	}
	json.Unmarshal(contents, &userConfig)
	mergeConfig(DefaultConfig, &userConfig)

	//Apply 1 pass of aliases only
	for src, dst := range DefaultConfig.Aliases {
		destPort, found := DefaultConfig.PortMap[dst]
		if !found {
			log.Print("Key ", dst, " not found in port mappings so alias ", src, "will not be created")
			continue
		}
		DefaultConfig.PortMap[src] = destPort
	}
	log.Print("Sucessfully loaded user config.")
}
